#ifndef KALMAN_XY_POS_HPP
#define KALMAN_XY_POS_HPP

/** @file kalman_xy_pos.hpp
 *  \brief Header file for the KalmanXYPos class.
 *  Details.
 */

#include <ros/ros.h>

//Lib newmat
#include <newmat/newmat.h>  // para matrices
//#include <newmat/newmatap.h>


/**
 * @class KalmanXYPos
 * \brief Kalman filter used for prediction of xy position
 */
class KalmanXYPos{

public:
    /**
     * Class constructor.
     */
    KalmanXYPos();
    /**
     * Class constructor, including parameters.
     */
   // KalmanXYPos(double observation_covariance, double model_covariance);

    ~KalmanXYPos();


    /// Predict step of the filter.
    void predict();

    /// Predict step of the filter, using a different time_step
    void predict(float measured_time_step);

    /// Predict step, for use only when predicting future traj.
    void predictPred();

    /**
     * Updates the filter state
     * @param obs_postion Vector of observations. Provides the measurement from the sensors.
     */
    void update(NEWMAT::ColumnVector obs_position);

    /// Correction step of the filter.
    void correct();

    /// Correction step, used only when predicting future traj.
    void correctPred();

    /// Initializes the filter
    void initializeFilter(NEWMAT::ColumnVector init_position);

    /// Stores the current state of the filter
    NEWMAT::ColumnVector curr_state_x;

    /// Used for the prediction steps
    NEWMAT::ColumnVector pred_state_x;

    /**
     * Covariance Matrix P.
     * x is a vector of dimension n, so P is n by n,
     * Down the diagonal of P, we find the variances of the elements of x.
     */

    NEWMAT::Matrix cov_mat_P;

    /// Covariance matrix used for the prediction steps
    NEWMAT::Matrix cov_mat_P_pred;

    /// whether or not the object is moving.
    bool parado;

    double time_step;

    double observation_covariance;

    double model_covariance;


private:


    // Kalman filter matrices

    /// Indentity matrix used for computation
    NEWMAT:: Matrix mat_I;

    /// Update matrix ( A ). Models the system over time, and relates a new model state against the previuos one.
    NEWMAT::Matrix mat_A;

    /// Covariance of the input.
    NEWMAT::Matrix mat_Q;

    /// Covariance matrix of the measurement vector Z
    NEWMAT::Matrix mat_R;


    /// Kalman gain matrix.
    NEWMAT::Matrix mat_K;

    /**
     * The Extraction matrix.
     * The matrix H tells us what sensor readings we'd get if x were the true state of affairs and our sensors were perfect
     */
    NEWMAT::Matrix mat_H;

    /// Difference between the observation and the kalman filter prediction.
    NEWMAT::ColumnVector col_vec_Y;


    /// Stores a previous observation.
    NEWMAT::ColumnVector col_vel_Z0; //apoyos para el update

};

#endif // KALMAN_XY_POS_HPP
