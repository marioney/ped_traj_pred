#ifndef PREDICTIONHDETECT_HPP
#define PREDICTIONHDETECT_HPP

#include <ros/ros.h>
#include <ped_traj_pred/predict_filter.hpp>
#include <hdetect/HumansFeatClass.h>
#include <hdetect/HumansFeat.h>
#include <vector>

class PredictionHDetect
{
  public:
    PredictionHDetect();
    ~PredictionHDetect();

    std::vector<PredictFilter*> predict_filter_vec;

  private:

    /// Time at previou measure
    std::vector<double> t_prev_measure_vec;
    int n_filters;
    double deletion_timeout;


    void callbackHDdetect(const hdetect::HumansFeatClassConstPtr& detections);
    bool checkTimeOut(double time_callback, int filter_vec_pos);
    int checkNewDetection(int human_id);
    void createNewFilter(hdetect::HumansFeat detection, double time_callback, std::string frame_id_det);
    void updateFilter(hdetect::HumansFeat detection, double filter_index, double time_callback);
    void deleteFilter(double filter_index);
    int checkDissapear(double time_callback);

    ros::Subscriber hdetec_sub;

};

#endif // PREDICTIONHDETECT_HPP
