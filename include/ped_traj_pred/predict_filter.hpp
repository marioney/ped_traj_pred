#ifndef PREDICTFILTER_H
#define PREDICTFILTER_H

#include <ros/ros.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <ped_traj_pred/kalman_xy_pos.hpp>

//Position messages
#include <visualization_msgs/Marker.h>

// Path with id to RiskRRT planner
#include <ped_traj_pred/PathWithId.h> //Path with id

class PredictFilter : KalmanXYPos
{
  public:
    PredictFilter();
    ~PredictFilter();

    void publish_spin(const ros::TimerEvent& e);
    void refresh_pos(float pos_x, float pos_y);

    bool filter_initialized;
    std::string output_frame_id;
    //KalmanXYPos kalman_xy_pos;

    double getTime_step() const;
    void setTime_step(double value);

    ros::Timer publish_timer;

    int getPath_id() const;
    void setPath_id(int value);

  private:

    int num_pos;

    int path_id;

    ros::Publisher  visualization_pub;
    ros::Publisher  future_pos_pub;
    ros::Publisher	future_path_pub;


    ////////////////////////////////////////////////////
    visualization_msgs::Marker points, line_strip; //aki esta el problem
    geometry_msgs::Point mark; //variable auxiliar para la visualizacion
    geometry_msgs::PoseWithCovarianceStamped poswithcov;

};

#endif // PREDICTFILTER_H
