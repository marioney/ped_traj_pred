#include <ros/ros.h>
#include <std_msgs/String.h>
#include <math.h>
#include <string>

//Position messages

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Odometry.h>

//Lib newmat
#include <newmat/newmat.h>  // para matrices
#include <newmat/newmatap.h>

#include <visualization_msgs/Marker.h>
// Path with id to RiskRRT planner
#include <ped_traj_pred/PathWithId.h> //Path with id

#include <ros/callback_queue.h>
#include <fstream> // archivos
#include <iostream>



using namespace  NEWMAT;

class kalman_xy_pos {


public:

    kalman_xy_pos();
    ~kalman_xy_pos();

	
    void kal_predict();
    void kal_predict_pred();
    void kal_update(ColumnVector obs_position);
    void kal_correct();
    void kal_correct_pred();
	void Callback_pos(const geometry_msgs::PoseStampedConstPtr& Pos);
    void Callback_posCov(const geometry_msgs::PoseWithCovarianceStampedConstPtr& PosCov);
    void Callback_odometry(const nav_msgs::OdometryConstPtr& odom);
    void publish_spin(const ros::TimerEvent& e);
    void refresh_pos(float pos_x, float pos_y);
    void initialize_filter(ColumnVector init_position);
	
    ColumnVector state_vec_x;

    ColumnVector state_vec_x_pred; // Donde guardamos el primer valor obtenido
    //ColumnVector m; // Donde guardamos la medicion en estructura matricial
	

    Matrix cov_mat_P;
    Matrix cov_mat_P_pred;
	//float P[MAX][MAX]; 
	// Covarianza del Error (prediccion ->) P = A*P*At + Q // (correccion ->) P = (I - K*H)P	


	
	//COSAS
    double time_step;
    int num_pos; // Parametros incluidos en el launcher

    double cov_a; // Noise covariance of the model

    std_msgs::UInt8 path_id;

	
	std::ofstream pred_file_; //archivo para el Debug
		
    bool flag;
    double t_prev_measure;
	string frame;

	bool parado;

    ros::Publisher  visualization_pub;
    ros::Publisher  future_pos_pub;
    ros::Publisher	future_path_pub;
    ros::Subscriber pose_sub;
    ros::Subscriber	pose_with_cov_sub;
    ros::Subscriber odometry_pose_sub;
    ros::Timer publish_timer;

    ////////////////////////////////////////////////////
    visualization_msgs::Marker points, line_strip; //aki esta el problem
    geometry_msgs::Point mark; //variable auxiliar para la visualizacion
    geometry_msgs::PoseWithCovarianceStamped poswithcov;


private:  //Debemos hacer todas la operaciones en funciones para poder hacerlo privado y NO usarlo en el main.
	
    // ROS Publishers and subscribers



	//MATRICES

    Matrix mat_I;
        
    Matrix mat_A;
	
    Matrix mat_Q;
	
    Matrix mat_R;
	
    Matrix mat_K;
		
    Matrix mat_H;
		
    ColumnVector col_vec_Y;


	

	
    ColumnVector col_vel_Z0; //apoyos para el update

};
