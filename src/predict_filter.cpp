#include <ped_traj_pred/predict_filter.hpp>

using NEWMAT::Matrix;
using NEWMAT::ColumnVector;

PredictFilter::PredictFilter()
{ //Constructor

  ROS_INFO("[PredictFilter] -- constructor ");

  ros::NodeHandle  nh("~");  // Create a ROS nodehandle

  std::string visualization_topic_name;
  std::string future_pose_topic_name;
  std::string future_path_topic_name;

  // Entrada de parámetros //

  double path_time;
  double pub_freq;


  nh.param("path_time", path_time, 6.0);
  nh.param("visualization_topic", visualization_topic_name,std::string("visualizacion_kalman"));
  nh.param("future_pose_topic", future_pose_topic_name,std::string("future_pos"));
  nh.param("future_path_topic", future_path_topic_name,std::string("kalman_pre_path"));
  nh.param("pub_freq",pub_freq, 1.0);

    ROS_INFO("[PredictFilter] -- Advertise topics ");

//  kalman_xy_pos = KalmanXYPos(time_step, cov_obs, cov_model);
  //KalmanXYPos(time_step, cov_obs, cov_model);
  visualization_pub = nh.advertise<visualization_msgs::Marker>(visualization_topic_name, 1);
  future_pos_pub = nh.advertise<geometry_msgs::PoseWithCovarianceStamped> (future_pose_topic_name, 1);
  future_path_pub = nh.advertise<ped_traj_pred::PathWithId>(future_path_topic_name, 1);

  num_pos = (int) (path_time / time_step);
  filter_initialized = false;


  // VISUALIZACION

  points.ns = line_strip.ns = "posicion_futura";
  points.action = line_strip.action = visualization_msgs::Marker::ADD;

  //points.lifetime = line_strip.lifetime = ros::Duration();

  points.id = 0;
  line_strip.id = 1;

  points.type = visualization_msgs::Marker::POINTS;
  line_strip.type = visualization_msgs::Marker::LINE_STRIP;

  points.scale.x = 0.2;
  points.scale.y = 0.2;

  line_strip.scale.x = 0.1;
  line_strip.scale.y = 0.1;

  points.color.r = 1.0;
  points.color.g = 1.0;
  points.color.a = 1.0;
  line_strip.color.b = 1.0;
  line_strip.color.g = 1.0;
  line_strip.color.a = 1.0;


  publish_timer = nh.createTimer(ros::Duration(1.0/pub_freq), &PredictFilter::publish_spin, this);
  publish_timer.stop();


  ROS_INFO("[PredictFilter] -- Init oK - n pos = %d ", num_pos);
}

 PredictFilter::~PredictFilter() { //Destructor


}

 void PredictFilter::publish_spin(const ros::TimerEvent& e)
 {

   //  int pos_counter;
   //  ROS_INFO("[publish-spin] -- in");

     //POSESTAMPED

     geometry_msgs::PoseWithCovarianceStamped poswithcov;
     poswithcov.header.frame_id = output_frame_id;
     poswithcov.header.stamp = ros::Time::now();

     //PATH
     ped_traj_pred::PathWithId kalPath;
     kalPath.path.poses.clear();
     kalPath.path.poses.resize(num_pos);
     kalPath.path.header.frame_id = output_frame_id;
     kalPath.path.header.stamp = ros::Time::now();
     kalPath.path_id.data = path_id;
     points.header.stamp = line_strip.header.stamp = ros::Time();
     points.header.frame_id = line_strip.header.frame_id = output_frame_id;
     points.points.clear();
     line_strip.points.clear();

     // De aki obtenemos el valor de la primera medición
     pred_state_x = curr_state_x;
     cov_mat_P_pred = cov_mat_P;

     ros::Time estimation_stamp_;

     estimation_stamp_ = ros::Time::now();

     //PREDICCION

     for(unsigned int pos_counter = 0; pos_counter <= num_pos ; pos_counter++)
     {
         if (pos_counter < num_pos)
         {
             estimation_stamp_ = estimation_stamp_ + ros::Duration(time_step);
             predictPred();
             correctPred();


             //Publicaciones
             //VISUALIZACION
             mark.x = pred_state_x.element(0);
             mark.y = pred_state_x.element(1);
             mark.z = 0;

             points.points.push_back(mark);
             line_strip.points.push_back(mark);

             visualization_pub.publish(points);
             visualization_pub.publish(line_strip);

             //POSESTAMPED   => hay que poner la covarianza y los tiempos etc.
             poswithcov.header.stamp = poswithcov.header.stamp + ros::Duration(time_step);
             poswithcov.pose.pose.position.x = pred_state_x.element(0);
             poswithcov.pose.pose.position.y = pred_state_x.element(1);
             future_pos_pub.publish(poswithcov); // aki se publica la posicion futura.


             //PATH
             geometry_msgs::PoseStamped pose_at_x;
             pose_at_x.header.stamp = estimation_stamp_;
             pose_at_x.header.frame_id = output_frame_id;

             pose_at_x.pose.orientation.w = 1;
             pose_at_x.pose.position.x = pred_state_x.element(0);
             pose_at_x.pose.position.y = pred_state_x.element(1);
             kalPath.path.poses[pos_counter] = pose_at_x;
         }
         else if (pos_counter == num_pos)
         {

             future_path_pub.publish(kalPath);  //PUBLICAMOS LA TRAYECTORIA PREDECIDA CON UN PATH with ID


         }

     }
     // Necesario para hacer la diferencia entre la primera prediccion y la medida real
  //   ROS_INFO("[publish-spin] -- out");
 }



void PredictFilter::refresh_pos(float pos_x, float pos_y)
{
    ColumnVector recv_position = ColumnVector(2);

    //ROS_INFO("[refres_pos] - obs_position  x = %f - y = %f", pos_x, pos_y);

    recv_position.element(0) = pos_x;
    recv_position.element(1) = pos_y;

    if ( filter_initialized == true)
    {

      predict();

      update(recv_position);

      correct();

    }
    else
    {

      initializeFilter(recv_position); //OK
      filter_initialized = true;
    }


}
double PredictFilter::getTime_step() const
{
  return time_step;
}

void PredictFilter::setTime_step(double value)
{
  time_step = value;
}
int PredictFilter::getPath_id() const
{
  return path_id;
}

void PredictFilter::setPath_id(int value)
{
  path_id = value;
}

