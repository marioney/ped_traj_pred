#include <ped_traj_pred/ped_traj_pred.hpp>

PedTrajPred::PedTrajPred()
{

  ros::NodeHandle  nh("~");  // Create a ROS nodehandle

  // Store topic names
  std::string pose_topic_name;
  std::string pose_with_cov_topic_name;
  std::string odometry_pose_topic_name;

  int path_id_param;

  nh.param("path_id", path_id_param, 1);

  nh.param("pose_topic", pose_topic_name,std::string("/world_estimated_position"));
  nh.param("pose_with_cov_topic", pose_with_cov_topic_name,std::string("/amcl_poselll"));
  nh.param("odometry_pose_topic", odometry_pose_topic_name,std::string("/odom"));

  pose_sub = nh.subscribe(pose_topic_name,1, &PedTrajPred::Callback_pos, this); //Suscripcion a la posicion que manda el quadrotor!
  pose_with_cov_sub = nh.subscribe(pose_with_cov_topic_name,1, &PedTrajPred::Callback_posCov, this); // Suscripcion con covarianzas
  odometry_pose_sub = nh.subscribe(odometry_pose_topic_name,1, &PedTrajPred::Callback_odometry, this); // Suscripcion con covarianzas

  predict_filter = new PredictFilter();

  predict_filter->setPath_id(path_id_param);

  ROS_INFO("[PedTraPred] -- init OK!");

  }

PedTrajPred::~PedTrajPred()
{ //Destructor

  delete predict_filter;


}


void PedTrajPred::Callback_pos(const geometry_msgs::PoseStampedConstPtr& Pos)
// Obtencion de la posicion y convertirla al vector de medicion
{
    //frame = Pos->header.frame_id; // esto en caso normal
    predict_filter->output_frame_id = "/map";				// del gps ya que el frame no es el mapa
    double time_callback;
    time_callback = Pos->header.stamp.sec + Pos->header.stamp.nsec*1E-9;


 //   ROS_INFO("[pos] TIEMPO: %f", t-t0);

    predict_filter->refresh_pos(Pos->pose.position.x, Pos->pose.position.y);

}

void PedTrajPred::Callback_odometry(const nav_msgs::OdometryConstPtr& odom)
{
    // Obtencion de la posicion y convertirla al vector de medicion
    predict_filter->output_frame_id = odom->header.frame_id;
    double time_callback;
    time_callback = odom->header.stamp.sec + odom->header.stamp.nsec*1E-9;
    if (time_callback - t_prev_measure < predict_filter->getTime_step())
    {
        return;
    }

    //ROS_INFO("[odometry] -  TIEMPO: %f", time_callback - t_prev_measure);
    predict_filter->refresh_pos(odom->pose.pose.position.x, odom->pose.pose.position.y);
    t_prev_measure =  time_callback;
}

void PedTrajPred::Callback_posCov(const geometry_msgs::PoseWithCovarianceStampedConstPtr& PosCov){

    predict_filter->output_frame_id = PosCov->header.frame_id; // esto en caso normal
    //frame = "/map";				// del gps ya que el frame no es el mapa
    double time_callback;
    time_callback = PosCov->header.stamp.sec + PosCov->header.stamp.nsec*1E-9;


 //   ROS_INFO("[pos with cov]  TIEMPO: %f", t-t0);


    predict_filter->refresh_pos(PosCov->pose.pose.position.x, PosCov->pose.pose.position.y);

}


int main(int argc, char **argv){

  ros::init(argc, argv, "Predict_Step");

  ROS_INFO("[PED TRAJ PRED] - Start node");
  PedTrajPred Pos_Kal; // Inicializacion

  ros::Rate ros_rate(4);

  sleep(5);


  while (Pos_Kal.predict_filter->filter_initialized == false && ros::ok())
  {
      //Espera hasta tener un valor que incluir en z
      ros::spinOnce();
      ros_rate.sleep();
      ROS_INFO("Waiting for any position message");
  }

  Pos_Kal.predict_filter->publish_timer.start();
  ros::spin();
}
