#include <ped_traj_pred/kalman_xy_pos.hpp>

using NEWMAT::Matrix;
using NEWMAT::ColumnVector;
using NEWMAT::IdentityMatrix;

KalmanXYPos::KalmanXYPos()
{


  ros::NodeHandle  nh_in("~");  // Create a ROS nodehandle

  nh_in.param("model_covariance", model_covariance, 2.0);
  nh_in.param("observation_covariance", observation_covariance, 1.0);
  nh_in.param("time_step", time_step, 0.5); // Hay que poner el namespace para poder cargarlo

// Initialize kalman varialbes

  int state_vec_size = 4;
  int observation_vec_size = 2;

//

  mat_I = IdentityMatrix(state_vec_size);

  cov_mat_P = Matrix(state_vec_size, state_vec_size);

  cov_mat_P_pred = Matrix(state_vec_size, state_vec_size);

  //mat_R = Matrix(observation_vec_size, observation_vec_size);

  mat_H = Matrix(observation_vec_size, state_vec_size);

  mat_K = Matrix(state_vec_size, observation_vec_size);

  col_vec_Y = ColumnVector(observation_vec_size);

  cov_mat_P = mat_I;

  cov_mat_P_pred = mat_I;


  mat_R = IdentityMatrix(observation_vec_size);
  mat_R = mat_R * observation_covariance;


  mat_H << 1 << 0 << 0 << 0 <<
           0 << 1 << 0 << 0;

  mat_K  << 1 << 0 <<
            0 << 1 <<
            0 << 0 <<
            0 << 0;

  col_vec_Y << 0 <<
               0;

  // Model initialization
//  ROS_INFO("[KALMAN XY POS] - Kalman Model initialization");
  mat_A = Matrix(state_vec_size, state_vec_size);
  mat_Q = Matrix(state_vec_size, state_vec_size);

  ColumnVector col_vec_G;

  col_vec_G = ColumnVector(observation_vec_size);
  Matrix Q_top  = Matrix(observation_vec_size, observation_vec_size);

  //    col_vec_G = ColumnVector(4);
  //    Matrix Q_top  = Matrix(4, 4);

  mat_A << 1 << 0 << time_step << 0 <<
           0 << 1 << 0 << time_step <<
           0 << 0 << 1 << 0 <<
           0 << 0 << 0 << 1;

  double delta_t_2;

  delta_t_2 = pow(time_step,2.0);
  delta_t_2 = delta_t_2/2;
  col_vec_G << delta_t_2 <<
               time_step;


//    col_vec_G << delta_t_2 <<
//                 delta_t_2 <<
//                 time_step <<
//                 time_step;

//  ROS_INFO("Delta t ^ 2 / = %f,   -  cov a = %f ", delta_t_2, model_covariance );


  Q_top = col_vec_G*col_vec_G.t();
  Q_top = Q_top * model_covariance;

  mat_Q  << Q_top(1,1) << 0 << 0 << 0 <<
            0 << Q_top(2,2) << 0 << 0 <<
            0 << 0 << Q_top(1,1) << 0 <<
            0 << 0 << 0 << Q_top(2,2);

//  mat_Q  << 0.15 * time_step << 0 << 0 << 0 <<
//            0 << 0.15 * time_step << 0 << 0 <<
//            0 << 0 << 0.30 * time_step << 0 <<
//            0 << 0 << 0 << 0.30 * time_step;

  ROS_INFO("[KALMAN XY POS] - Kalman filter init OK!");
}

KalmanXYPos::~KalmanXYPos()
{
  //Destructor

}


void KalmanXYPos::predict()
{
  cov_mat_P = mat_A * cov_mat_P * mat_A.t() + mat_Q;

  //  float Det_P;
  //  Det_P = cov_mat_P.Determinant();
  //  ROS_INFO("[predict] - Determinante  P : %f", Det_P);

  if (!parado)
  {
    curr_state_x = mat_A * curr_state_x;
  }
}

void KalmanXYPos::predict(float measured_time_step)
{
  Matrix mat_A_temp;
  Matrix mat_Q_temp;

  mat_A << 1 << 0 << measured_time_step << 0 <<
           0 << 1 << 0 << measured_time_step <<
           0 << 0 << 1 << 0 <<
           0 << 0 << 0 << 1;
  ColumnVector vec_G_temp = ColumnVector(2);
  Matrix Q_top_temp  =  Matrix(2, 2);

  //    col_vec_G = ColumnVector(4);
  //    Matrix Q_top  = Matrix(4, 4);


  double delta_t_2;

  delta_t_2 = pow(time_step,2.0);
  delta_t_2 = delta_t_2/2;
  vec_G_temp << delta_t_2 <<
                time_step;


//    col_vec_G << delta_t_2 <<
//                 delta_t_2 <<
//                 time_step <<
//                 time_step;

//  ROS_INFO("Delta t ^ 2 / = %f,   -  cov a = %f ", delta_t_2, model_covariance );


  Q_top_temp = vec_G_temp*vec_G_temp.t();
  Q_top_temp = Q_top_temp * model_covariance;

  mat_Q_temp  << Q_top_temp(1,1) << 0 << 0 << 0 <<
                 0 << Q_top_temp(2,2) << 0 << 0 <<
                 0 << 0 << Q_top_temp(1,1) << 0 <<
                 0 << 0 << 0 << Q_top_temp(2,2);

  cov_mat_P = mat_A_temp * cov_mat_P * mat_A_temp.t() + mat_Q_temp;

  //  float Det_P;
  //  Det_P = cov_mat_P.Determinant();
  //  ROS_INFO("[predict] - Determinante  P : %f", Det_P);

  if (!parado)
  {
    curr_state_x = mat_A_temp * curr_state_x;
  }
}
void KalmanXYPos::predictPred()
{
  cov_mat_P_pred = mat_A * cov_mat_P_pred * mat_A.t() + mat_Q;

  // float Det_P;
  // Det_P = cov_mat_P_pred.Determinant();
  // ROS_INFO("Determinante  Pred : %f", Det_P);

  if (!parado)
  {
    pred_state_x = mat_A *  pred_state_x;
  }
}

void KalmanXYPos::correctPred()
{
  pred_state_x = pred_state_x + mat_K * col_vec_Y; // Valor predecido!  ******
  cov_mat_P_pred = (mat_I - mat_K * mat_H) * cov_mat_P_pred; //
  //ROS_INFO("Pos Predicha x: %f, %f, %f, %f", x.element(0), x.element(1), x.element(2), x.element(3) );
}

void KalmanXYPos::correct()
{
  curr_state_x = curr_state_x + mat_K * col_vec_Y; // Valor predecido!  ******
  cov_mat_P = (mat_I - mat_K * mat_H) * cov_mat_P; // Correción de la covarianza
  // ROS_INFO("Pos Predichax: %f, %f, %f, %f", x.element(0), x.element(1), x.element(2), x.element(3) );
}


void KalmanXYPos::update(ColumnVector obs_position)
{
  float modZ;
  Matrix mat_S;

  modZ = (obs_position - col_vel_Z0).NormFrobenius();
  if(modZ <= 0.01 || modZ <= 0.01)
  {
    col_vec_Y << 0 <<
                 0;
    parado = true;
    //ROS_INFO("[KALMANPOS - update] - no_motion");
  }
  else
  {
    col_vec_Y = obs_position - mat_H * curr_state_x; //lo que introduzcamos en forma de matriz
    parado = false;
  }

  mat_S = mat_H * cov_mat_P * mat_H.t() + mat_R;
  mat_K = cov_mat_P * mat_H.t() * mat_S.i();

  col_vel_Z0 = obs_position;
}


void KalmanXYPos::initializeFilter(ColumnVector init_position)
{
  col_vel_Z0 = ColumnVector(2);
  curr_state_x = ColumnVector(4);
  pred_state_x = ColumnVector(4);
  curr_state_x  << init_position.element(0) <<
                   init_position.element(1) <<
                   0 <<
                   0;

  pred_state_x = curr_state_x;

  col_vel_Z0 = init_position;
  ROS_INFO("[KALMAN XY POS - initializeFilter] Posicion inicial x: %f, %f, %f, %f",
           curr_state_x.element(0), curr_state_x.element(1),
           curr_state_x.element(2), curr_state_x.element(3) );
}
