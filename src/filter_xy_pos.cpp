#include <ped_traj_pred/filter_xy_pos.hpp>

using namespace  NEWMAT;

kalman_xy_pos::kalman_xy_pos() { //Constructor



 ros::NodeHandle  nh("~");  // Create a ROS nodehandle

 // Store topic names
 std::string pose_topic_name;
 std::string pose_with_cov_topic_name;
 std::string odometry_pose_topic_name;
 std::string visualization_topic_name;
 std::string future_pose_topic_name;
 std::string future_path_topic_name;

 ///// Entrada de parámetros ///////
 double path_time;
 double pub_freq;
 int path_id_param;

 nh.param("time_step", time_step, 0.5); // Hay que poner el namespace para poder cargarlo
 nh.param("path_time", path_time, 6.0);
 nh.param("path_id", path_id_param, 1);
 nh.param("pose_topic", pose_topic_name,std::string("/world_estimated_position"));
 nh.param("pose_with_cov_topic", pose_with_cov_topic_name,std::string("/amcl_poselll"));
 nh.param("odometry_pose_topic", odometry_pose_topic_name,std::string("/odom"));
 nh.param("visualization_topic", visualization_topic_name,std::string("visualizacion_kalman"));
 nh.param("future_pose_topic", future_pose_topic_name,std::string("future_pos"));
 nh.param("future_path_topic", future_path_topic_name,std::string("kalman_pre_path"));

 nh.param("model_covariance", cov_a, 10.0);
 
 nh.param("pub_freq",pub_freq, 1.0);


 pose_sub = nh.subscribe(pose_topic_name,1, &kalman_xy_pos::Callback_pos, this); //Suscripcion a la posicion que manda el quadrotor!
 pose_with_cov_sub = nh.subscribe(pose_with_cov_topic_name,1, &kalman_xy_pos::Callback_posCov, this); // Suscripcion con covarianzas
 odometry_pose_sub = nh.subscribe(odometry_pose_topic_name,1, &kalman_xy_pos::Callback_odometry, this); // Suscripcion con covarianzas
 visualization_pub = nh.advertise<visualization_msgs::Marker>(visualization_topic_name, 1);
 future_pos_pub = nh.advertise<geometry_msgs::PoseWithCovarianceStamped> (future_pose_topic_name, 1);
 future_path_pub = nh.advertise<ped_traj_pred::PathWithId>(future_path_topic_name, 1);

 t_prev_measure = 0;

 num_pos = (int) (path_time / time_step);

 path_id.data = path_id_param;

/////////////COSAS/////////////
 
flag = false;
	
//////////////MATRICES//////////////////
   
    mat_I = IdentityMatrix(4);

    cov_mat_P = Matrix(4, 4);

    cov_mat_P_pred = Matrix(4, 4);

    mat_R = Matrix(2, 2);
    mat_H = Matrix(2, 4);
    
    mat_K = Matrix(4, 2);
    col_vec_Y = ColumnVector(2);


    cov_mat_P = mat_I;

    cov_mat_P_pred = mat_I;

    mat_R << 0.2 << 0 <<
              0 << 0.2;
         
    mat_H << 1 << 0 << 0 << 0 <<
             0 << 1 << 0 << 0;

    mat_K  << 1 << 0 <<
              0 << 1 <<
              0 << 0 <<
              0 << 0;

    col_vec_Y << 0 <<
                 0;


// Model initialization

    mat_A = Matrix(4, 4);
    mat_Q = Matrix(4, 4);
    ColumnVector col_vec_G;

    col_vec_G = ColumnVector(2);
    Matrix Q_top  = Matrix(2, 2);

//    col_vec_G = ColumnVector(4);
//    Matrix Q_top  = Matrix(4, 4);

    mat_A << 1 << 0 << time_step << 0 <<
             0 << 1 << 0 << time_step <<
             0 << 0 << 1 << 0 <<
             0 << 0 << 0 << 1;

    double delta_t_2;

    delta_t_2 = pow(time_step,2.0);
    delta_t_2 = delta_t_2/2;

    col_vec_G << delta_t_2 <<
                 time_step;


//    col_vec_G << delta_t_2 <<
//                 delta_t_2 <<
//                 time_step <<
//                 time_step;

    ROS_INFO("Delta t ^ 2 / = %f,   -  cov a = %f ", delta_t_2, cov_a );

    Q_top = col_vec_G*col_vec_G.t();
    Q_top = Q_top * cov_a;

//    mat_Q = Q_top;

//    mat_Q  << Q_top(1,1) << 0 << 0 << 0 <<
//              0 << Q_top(2,2) << 0 << 0 <<
//              0 << 0 << Q_top(1,1) << 0 <<
//              0 << 0 << 0 << Q_top(2,2);

    mat_Q  << 0.15 * time_step << 0 << 0 << 0 <<
              0 << 0.15 * time_step << 0 << 0 <<
              0 << 0 << 0.30 * time_step << 0 <<
              0 << 0 << 0 << 0.30 * time_step;

	
///////////////////////////////////
//VISUALIZACION
/////////////////////////////////////////////////////



    points.ns = line_strip.ns = "posicion_futura";
    points.action = line_strip.action = visualization_msgs::Marker::ADD;
    //points.lifetime = line_strip.lifetime = ros::Duration();

    points.id = 0;
    line_strip.id = 1;

    points.type = visualization_msgs::Marker::POINTS;
    line_strip.type = visualization_msgs::Marker::LINE_STRIP;

    points.scale.x = 0.2;
    points.scale.y = 0.2;

    line_strip.scale.x = 0.1;
    line_strip.scale.y = 0.1;

    points.color.r = 1.0;
    points.color.g = 1.0;
    points.color.a = 1.0;

    line_strip.color.b = 1.0;
    line_strip.color.g = 1.0;
    line_strip.color.a = 1.0;


    publish_timer = nh.createTimer(ros::Duration(1.0/pub_freq), &kalman_xy_pos::publish_spin, this);
    publish_timer.stop();

    ROS_INFO("[Constructor] -- n pos = %d ", num_pos);
    ROS_INFO("Kalman filter init OK!");

  }
  
 kalman_xy_pos::~kalman_xy_pos() { //Destructor
	

}

 void kalman_xy_pos::publish_spin(const ros::TimerEvent& e)
 {

   //  int pos_counter;
   //  ROS_INFO("[publish-spin] -- in");

     //POSESTAMPED

     geometry_msgs::PoseWithCovarianceStamped poswithcov;
     poswithcov.header.frame_id = frame;
     poswithcov.header.stamp = ros::Time::now();

     //PATH
     ped_traj_pred::PathWithId kalPath;
     kalPath.path.poses.clear();
     kalPath.path.poses.resize(num_pos);
     kalPath.path.header.frame_id = frame;
     kalPath.path.header.stamp = ros::Time::now();
     kalPath.path_id = path_id;
     points.header.stamp = line_strip.header.stamp = ros::Time();
     points.header.frame_id = line_strip.header.frame_id = frame;
     points.points.clear();
     line_strip.points.clear();

     // De aki obtenemos el valor de la primera medición
     state_vec_x_pred = state_vec_x;
     cov_mat_P_pred = cov_mat_P;

     ros::Time estimation_stamp_;

     estimation_stamp_ = ros::Time::now();

     //PREDICCION

     for(unsigned int pos_counter = 0; pos_counter <= num_pos ; pos_counter++)
     {
         if (pos_counter < num_pos)
         {
             estimation_stamp_ = estimation_stamp_ + ros::Duration(time_step);
             kal_predict_pred();
           //  kal_correct_pred();

             //Publicaciones
             //VISUALIZACION
             mark.x = state_vec_x_pred.element(0);
             mark.y = state_vec_x_pred.element(1);
             mark.z = 0;

             points.points.push_back(mark);
             line_strip.points.push_back(mark);

             visualization_pub.publish(points);
             visualization_pub.publish(line_strip);

             //POSESTAMPED   => hay que poner la covarianza y los tiempos etc.
             poswithcov.header.stamp = poswithcov.header.stamp + ros::Duration(time_step);
             poswithcov.pose.pose.position.x = state_vec_x_pred.element(0);
             poswithcov.pose.pose.position.y = state_vec_x_pred.element(1);
             future_pos_pub.publish(poswithcov); // aki se publica la posicion futura.


             //PATH
             geometry_msgs::PoseStamped pose_at_x;
             pose_at_x.header.stamp = estimation_stamp_;
             pose_at_x.header.frame_id = frame;

             pose_at_x.pose.orientation.w = 1;
             pose_at_x.pose.position.x = state_vec_x_pred.element(0);
             pose_at_x.pose.position.y = state_vec_x_pred.element(1);
             kalPath.path.poses[pos_counter] = pose_at_x;
         }
         else if (pos_counter == num_pos)
         {

             future_path_pub.publish(kalPath);  //PUBLICAMOS LA TRAYECTORIA PREDECIDA CON UN PATH with ID


         }

     }
     // Necesario para hacer la diferencia entre la primera prediccion y la medida real
    // ROS_INFO("[publish-spin] -- out");
 }

//T:cada cuanto tiempo se predice, x_inic a partir de que valor se predice.
void kalman_xy_pos::kal_predict()
{
    float Det_P;
    cov_mat_P = mat_A * cov_mat_P * mat_A.t() + mat_Q;
  //  Det_P = cov_mat_P.Determinant();
  //  ROS_INFO("Determinante  P : %f", Det_P);

    if (!parado)
    {
       state_vec_x = mat_A * state_vec_x;
    }
}
void kalman_xy_pos::kal_predict_pred()
{
    float Det_P;
    cov_mat_P_pred = mat_A * cov_mat_P_pred * mat_A.t() + mat_Q;
   // Det_P = cov_mat_P_pred.Determinant();
   // ROS_INFO("Determinante  Pred : %f", Det_P);

    if (!parado)
    {
        state_vec_x_pred = mat_A *  state_vec_x_pred;
    }
}

void kalman_xy_pos::kal_correct_pred()
{
    state_vec_x_pred = state_vec_x_pred + mat_K * col_vec_Y; // Valor predecido!  ******
  //  cov_mat_P_pred = (mat_I - mat_K * mat_H) * cov_mat_P_pred; //
    //ROS_INFO("Pos Predicha x: %f, %f, %f, %f", x.element(0), x.element(1), x.element(2), x.element(3) );

}
void kalman_xy_pos::kal_correct()
{
    //ROS_INFO("[kal_correct] -- In");

    state_vec_x = state_vec_x + mat_K * col_vec_Y; // Valor predecido!  ******
    cov_mat_P = (mat_I - mat_K * mat_H) * cov_mat_P; // Correción de la covarianza

   // ROS_INFO("Pos Predichax: %f, %f, %f, %f", x.element(0), x.element(1), x.element(2), x.element(3) );

}

void kalman_xy_pos::kal_update(ColumnVector obs_position)
{
    float modZ;
    Matrix mat_S;
    modZ = (obs_position - col_vel_Z0).NormFrobenius();

    if(modZ <= 0.01 || modZ <= 0.01)
    {
        col_vec_Y << 0 <<
                     0;
        parado = true;
        ROS_INFO("[kal_update] - parado");
    }
    else
    {
        col_vec_Y = obs_position - mat_H * state_vec_x; //lo que introduzcamos en forma de matriz
        parado = false;
    }
    mat_S = mat_H * cov_mat_P * mat_H.t() + mat_R;
    mat_K = cov_mat_P * mat_H.t() * mat_S.i();

    //x = x + K*Y;      // Valor predecido!  ******
    //cov_mat_P = (mat_I - mat_K * mat_H) * cov_mat_P;

    col_vel_Z0 = obs_position;
}



void kalman_xy_pos::refresh_pos(float pos_x, float pos_y)
{
    ColumnVector obs_position = ColumnVector(2);

    obs_position.element(0) = pos_x;
    obs_position.element(1) = pos_y;

    if ( flag == true)
    {

        kal_predict();
 //       ROS_INFO("[refres_pos] -- predict");
        kal_update(obs_position);
 //       ROS_INFO("[refres_pos] -- update");
        kal_correct();
 //       ROS_INFO("[refres_pos] -- correct");
    }
    else
    {
        initialize_filter(obs_position); //OK
  //      ROS_INFO("[refres_pos] -- initialize");
        flag = true;
    }



 //   ROS_INFO("medida: %f, %f", m.element(0), m.element(1));

}

void kalman_xy_pos::initialize_filter(ColumnVector init_position)
{
    col_vel_Z0 = ColumnVector(2);
    state_vec_x = ColumnVector(4);
    state_vec_x_pred = ColumnVector(4);
    state_vec_x  << init_position.element(0) <<
                    init_position.element(1) <<
                    0 <<
                    0;

    state_vec_x = state_vec_x_pred;

    col_vel_Z0 = init_position;

    ROS_INFO("Posicion inicial x: %f, %f, %f, %f", state_vec_x.element(0), state_vec_x.element(1),
             state_vec_x.element(2), state_vec_x.element(3) );



}

void kalman_xy_pos::Callback_pos(const geometry_msgs::PoseStampedConstPtr& Pos)
// Obtencion de la posicion y convertirla al vector de medicion
{
    //frame = Pos->header.frame_id; // esto en caso normal
    frame = "/map";				// del gps ya que el frame no es el mapa
    double time_callback;
    time_callback = Pos->header.stamp.sec + Pos->header.stamp.nsec*1E-9;


 //   ROS_INFO("[pos] TIEMPO: %f", t-t0);

    refresh_pos(Pos->pose.position.x, Pos->pose.position.y);

}

void kalman_xy_pos::Callback_odometry(const nav_msgs::OdometryConstPtr& odom)
{
    // Obtencion de la posicion y convertirla al vector de medicion
    frame = odom->header.frame_id;
    double time_callback;
    time_callback = odom->header.stamp.sec + odom->header.stamp.nsec*1E-9;
    if (time_callback - t_prev_measure < time_step)
    {

        return;
    }

  //  ROS_INFO("[odometry] -  TIEMPO: %f", time_callback - t_prev_measure);
    refresh_pos(odom->pose.pose.position.x, odom->pose.pose.position.y);
    t_prev_measure =  time_callback;
}

void kalman_xy_pos::Callback_posCov(const geometry_msgs::PoseWithCovarianceStampedConstPtr& PosCov){

    frame = PosCov->header.frame_id; // esto en caso normal
    //frame = "/map";				// del gps ya que el frame no es el mapa
    double time_callback;
    time_callback = PosCov->header.stamp.sec + PosCov->header.stamp.nsec*1E-9;


 //   ROS_INFO("[pos with cov]  TIEMPO: %f", t-t0);


    refresh_pos(PosCov->pose.pose.position.x, PosCov->pose.pose.position.y);

}


int main(int argc, char **argv){
	
  ros::init(argc, argv, "Predict_Step");

  kalman_xy_pos Pos_Kal; // Inicializacion


  ros::Rate ros_rate(4);

  sleep(3);


  while (Pos_Kal.flag == false && ros::ok())
  {
      //Espera hasta tener un valor que incluir en z
      ros::spinOnce();
      ros_rate.sleep();
      ROS_INFO("Waiting for any position message");
  }

  Pos_Kal.publish_timer.start();
  ros::spin();
}

