#include <ped_traj_pred/prediction_hdetect.hpp>

PredictionHDetect::PredictionHDetect()
{
  ros::NodeHandle  nh("~");  // Create a ROS nodehandle

  // Store topic names
  std::string hdetect_topic_name;

  nh.param("hdetect_topic", hdetect_topic_name, std::string("/HumansDetected"));
  nh.param("deletion_timeout", deletion_timeout, 5.0);

  hdetec_sub =  nh.subscribe(hdetect_topic_name, 1, &PredictionHDetect::callbackHDdetect, this);
  n_filters = 0;

  ROS_INFO("[PedTraPred] -- init OK!");

}

PredictionHDetect::~PredictionHDetect()
{
  std::vector<PredictFilter*>::iterator filt_it;

  for(filt_it = predict_filter_vec.begin();
          filt_it != predict_filter_vec.end();
          filt_it++)
  {
    delete *filt_it;
  }
}



void PredictionHDetect::callbackHDdetect(const hdetect::HumansFeatClassConstPtr& detections)
{
  double time_callback;
  std::string frame_id_det;
  std::vector<hdetect::HumansFeat>::iterator detection_it;
  std::vector<hdetect::HumansFeat> detection_vec;
  int filter_index;

  time_callback = detections->header.stamp.sec + detections->header.stamp.nsec*1E-9;
  frame_id_det = detections->header.frame_id;

  detection_vec = detections->HumansDetected;

  for (detection_it = detection_vec.begin();
       detection_it != detection_vec.end();
       detection_it++)
  {
    filter_index = checkNewDetection((*detection_it).id);
    if (filter_index == -1)
    {
      ROS_INFO("[PRED HDETECT - callbackHDdetect] -- New detection!!  ID = %d", (*detection_it).id);
      createNewFilter(*detection_it, time_callback, frame_id_det);
    }
    else
    {
      if (checkTimeOut(time_callback, filter_index))
      {
        //ROS_INFO("[PRED HDETECT - callbackHDdetect] -- update detection!!  ID = %d", (*detection_it).id);
        updateFilter(*detection_it, filter_index, time_callback);
      }
    }
  }


  // Checks whether there are more filters than detections
  if((detection_it - detection_vec.begin()) < n_filters)
  {
    int dissapear_index;
    ROS_INFO("[PRED HDETECT - callbackHDdetect] -- There are %d filters and %d observations",
             n_filters, (int)(detection_it - detection_vec.begin()));
    dissapear_index = checkDissapear(time_callback);
   // ROS_INFO("[PRED HDETECT - callbackHDdetect] -- See me no more!! %d", dissapear_index);
    if ( dissapear_index != -1)
    {
      deleteFilter(dissapear_index);
    }

  }
}

void PredictionHDetect::deleteFilter(double filter_index)
{
  PredictFilter* filter_to_delete;
  filter_to_delete = predict_filter_vec.at(filter_index);
  delete filter_to_delete;
  std::swap(predict_filter_vec.at(filter_index), predict_filter_vec.back());
  std::swap(t_prev_measure_vec.at(filter_index), t_prev_measure_vec.back());
  predict_filter_vec.pop_back();
  t_prev_measure_vec.pop_back();
  n_filters --;
}
void PredictionHDetect::updateFilter(hdetect::HumansFeat detection, double filter_index, double time_callback)
{
  PredictFilter* filter_new;
  filter_new = predict_filter_vec.at(filter_index);
  filter_new->refresh_pos(detection.x, detection.y);
  t_prev_measure_vec.at(filter_index) = time_callback;
}

void PredictionHDetect::createNewFilter(hdetect::HumansFeat detection, double time_callback, std::string frame_id_det)
{
  PredictFilter* filter_new;
  filter_new = new PredictFilter();
  filter_new->setPath_id(detection.id);
  filter_new->output_frame_id = frame_id_det;
  filter_new->refresh_pos(detection.x, detection.y);
  filter_new->publish_timer.start();
  predict_filter_vec.push_back(filter_new);
  t_prev_measure_vec.push_back(time_callback);
  n_filters++;

}

int PredictionHDetect::checkDissapear(double time_callback)
{
  std::vector<double>::iterator time_out_it;

  for(time_out_it = t_prev_measure_vec.begin();
          time_out_it != t_prev_measure_vec.end();
          time_out_it++)
  {
    if (time_callback - *time_out_it > deletion_timeout)
    {
      int index_del;
      index_del = time_out_it - t_prev_measure_vec.begin();
      ROS_INFO("[PRED HDETECT - checkDissapear] -- Detection %d not present anymore",
               predict_filter_vec.at(index_del)->getPath_id());
      return (index_del);
    }
  }

  return (-1);
}


bool PredictionHDetect::checkTimeOut(double time_callback, int filter_vec_pos)
{
  PredictFilter* filt_it;
  //ROS_INFO("[PRED HDETECT - checkTimeout] -- filter vec pos: %d", filter_vec_pos);

  if (filter_vec_pos >= 0 && filter_vec_pos < predict_filter_vec.size())
  {
    filt_it = predict_filter_vec.at(filter_vec_pos);
    if (!(time_callback - t_prev_measure_vec.at(filter_vec_pos) < filt_it->getTime_step()))
    {
      return true;
    }
  }
  else
  {
    ROS_WARN("[PRED HDETECT - checkTimeout] -- Requested invalid filter position");
  }
  return false;
}

int PredictionHDetect::checkNewDetection(int human_id)
{
  std::vector<PredictFilter*>::iterator filt_it;

  for(filt_it = predict_filter_vec.begin();
          filt_it != predict_filter_vec.end();
          filt_it++)
  {
    if ((*filt_it)->getPath_id() == human_id)
    {
      //ROS_INFO("[PRED HDETECT - checkNewDetection] -- Detection %d already in the list", human_id);
      return (filt_it - predict_filter_vec.begin());
    }
  }

  return (-1);
}

int main(int argc, char **argv){

  ros::init(argc, argv, "Predict_Step");

  ROS_INFO("[PRED HDETECT] - Start node");
  PredictionHDetect Pos_Kal; // Inicializacion

  ros::Rate ros_rate(4);

  sleep(5);


//  while (Pos_Kal.predict_filter->filter_initialized == false && ros::ok())
//  {
//      //Espera hasta tener un valor que incluir en z
//      ros::spinOnce();
//      ros_rate.sleep();
//      ROS_INFO("Waiting for any position message");
//  }

//  Pos_Kal.predict_filter->publish_timer.start();
  ros::spin();
}
